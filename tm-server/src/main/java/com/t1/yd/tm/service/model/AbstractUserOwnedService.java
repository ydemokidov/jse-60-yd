package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.IUserOwnedRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.IUserOwnedService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends IUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    public AbstractUserOwnedService(@NotNull final ILoggerService loggerService) {
        super(loggerService);
    }

    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().add(userId, entity);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().clear(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findOneById(userId, id);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final E result = findOneById(userId, id);
        if (result == null) throw new EntityNotFoundException();
        getRepository().removeById(userId, id);
        return result;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final E result = findOneByIndex(userId, index);
        if (result == null) throw new EntityNotFoundException();
        getRepository().removeByIndex(userId, index);
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll(userId).size();
    }

}
