package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.ISessionRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.ISessionService;
import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    private final ISessionRepository repository;

    @Autowired
    public SessionService(@NotNull final ILoggerService loggerService,
                          @NotNull final ISessionRepository repository) {
        super(loggerService);
        this.repository = repository;
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public Session add(@NotNull String userId, @NotNull Session entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Session removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Session removeByIndex(@NotNull String userId, @NotNull Integer index) {
        return super.removeByIndex(userId, index);
    }

}
