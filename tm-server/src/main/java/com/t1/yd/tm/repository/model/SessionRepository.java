package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.api.repository.model.ISessionRepository;
import com.t1.yd.tm.model.Session;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Session> getClazz() {
        return Session.class;
    }

}
