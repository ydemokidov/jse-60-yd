package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoTaskRepository;
import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class TaskDtoRepository extends AbstractDtoUserOwnedRepository<TaskDTO> implements IDtoTaskRepository {

    @Override
    public @NotNull List<TaskDTO> findAllByProjectId(@NotNull final String userId, final @NotNull String projectId) {
        String query = String.format("FROM %s WHERE project_id = :project_id", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("project_id", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<TaskDTO> getClazz() {
        return TaskDTO.class;
    }

}
