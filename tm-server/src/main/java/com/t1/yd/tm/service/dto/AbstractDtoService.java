package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IDtoService;
import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoService<E extends AbstractEntityDTO, R extends IDtoRepository<E>> implements IDtoService<E> {

    @NotNull
    protected final ILoggerService loggerService;

    public AbstractDtoService(@NotNull final ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final E entity) {
        getRepository().add(entity);
        return entity;
    }

    @Override
    @SneakyThrows
    public void clear() {
        getRepository().clear();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return getRepository().findAll(comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        return getRepository().findOneByIndex(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final E result = findOneById(id);
        if (result == null) throw new EntityNotFoundException();
        getRepository().removeById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @Nullable final E result = findOneByIndex(index);
        if (result == null) throw new EntityNotFoundException();
        getRepository().removeByIndex(index);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(id) != null;
    }

    @Override
    @SneakyThrows
    public int getSize() {
        return getRepository().findAll().size();
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        getRepository().clear();
        collection.forEach(getRepository()::add);
        return collection;
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        collection.forEach(getRepository()::add);
        return collection;
    }

    @NotNull
    @Override
    public E update(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        getRepository().update(entity);
        return entity;
    }

}