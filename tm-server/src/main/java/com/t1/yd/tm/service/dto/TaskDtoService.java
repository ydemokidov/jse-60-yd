package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoTaskRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.ITaskDtoService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, IDtoTaskRepository> implements ITaskDtoService {

    private final IDtoTaskRepository repository;

    @Autowired
    public TaskDtoService(@NotNull final ILoggerService loggerService,
                          @NotNull final IDtoTaskRepository repository) {
        super(loggerService);
        this.repository = repository;
    }

    @NotNull
    @Override
    protected IDtoTaskRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public TaskDTO findTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    public TaskDTO findTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO taskDTO = findOneByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO removeTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO taskDTO = removeById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO removeTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO taskDTO = removeByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO(userId, name, description);
        return add(taskDTO);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setName(name);
        taskDTO.setDescription(description);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final TaskDTO taskDTO = findOneByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setName(name);
        taskDTO.setDescription(description);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setStatus(status);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO taskDTO = findOneByIndex(userId, index);
        if (taskDTO == null) throw new TaskNotFoundException();

        taskDTO.setStatus(status);
        update(taskDTO);

        return taskDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public TaskDTO add(@NotNull String userId, @NotNull TaskDTO entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeByIndex(@NotNull String userId, @NotNull Integer index) {
        return super.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@NotNull TaskDTO entity) {
        return super.add(entity);
    }

    @Override
    @Transactional
    public void clear() {
        super.clear();
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeById(@NotNull String id) {
        return super.removeById(id);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeByIndex(@NotNull Integer index) {
        return super.removeByIndex(index);
    }

    @Override
    @Transactional
    public @NotNull Collection<TaskDTO> set(@NotNull Collection<TaskDTO> collection) {
        return super.set(collection);
    }

    @Override
    @Transactional
    public @NotNull Collection<TaskDTO> add(@NotNull Collection<TaskDTO> collection) {
        return super.add(collection);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO update(@Nullable TaskDTO entity) {
        return super.update(entity);
    }

}