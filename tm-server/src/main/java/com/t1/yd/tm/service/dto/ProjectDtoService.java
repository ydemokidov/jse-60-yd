package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IProjectDtoService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IDtoProjectRepository> implements IProjectDtoService {

    private final IDtoProjectRepository repository;

    @Autowired
    public ProjectDtoService(@NotNull final ILoggerService loggerService,
                             @NotNull final IDtoProjectRepository repository) {
        super(loggerService);
        this.repository = repository;
    }

    @NotNull
    @Override
    protected IDtoProjectRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findOneById(id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    public ProjectDTO findProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return add(new ProjectDTO(userId, name, description));
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final ProjectDTO projectDTO = findOneById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setStatus(status);

        update(projectDTO);
        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > findAll(userId).size()) throw new IndexIncorrectException();
        @Nullable final ProjectDTO projectDTO = findOneByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();

        projectDTO.setStatus(status);
        update(projectDTO);

        return projectDTO;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO removeProjectById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable ProjectDTO projectDTO = removeById(userId, id);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = removeByIndex(userId, index);
        if (projectDTO == null) throw new ProjectNotFoundException();
        return projectDTO;
    }

    @Override
    @Transactional
    public ProjectDTO add(@NotNull String userId, @NotNull ProjectDTO entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeByIndex(@NotNull String userId, @NotNull Integer index) {
        return super.removeByIndex(userId, index);
    }

    @Override
    @Transactional
    public void clear() {
        super.clear();
    }


    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@NotNull ProjectDTO entity) {
        return super.add(entity);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> collection) {
        return super.set(collection);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> collection) {
        return super.add(collection);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO update(@Nullable ProjectDTO entity) {
        return super.update(entity);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeById(@NotNull String id) {
        return super.removeById(id);
    }

}