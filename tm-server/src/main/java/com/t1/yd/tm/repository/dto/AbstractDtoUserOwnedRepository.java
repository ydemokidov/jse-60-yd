package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoUserOwnedRepository;
import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import lombok.AllArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public abstract class AbstractDtoUserOwnedRepository<E extends AbstractUserOwnedEntityDTO> extends AbstractDtoRepository<E> implements IDtoUserOwnedRepository<E> {

    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        return add(entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String query = String.format("DELETE FROM %s where user_id = :userId", getEntityName());
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @NotNull List<E> findAll(@NotNull final String userId) {
        return findAll(userId, null);
    }

    @Override
    public @NotNull List<E> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final String query = String.format("FROM %s WHERE user_id = :userId ORDER BY %s", getEntityName(), getOrderByField(comparator));
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = String.format("FROM %s WHERE id = :id and user_id = :userId", getEntityName());
        return entityManager.createQuery(query, getClazz())
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = String.format("FROM %s where user_id = :userId ORDER BY %s", getEntityName(), getOrderByField(null));
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setFirstResult(index - 1)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findOneById(userId, id);
        if (entity == null) return null;
        return remove(entity);
    }

    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        return remove(entity);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String query = String.format("SELECT COUNT(1) FROM %s where user_id = :userId", getEntityName());
        @NotNull final Long count = entityManager.createQuery(query, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
        return Math.toIntExact(count);
    }

}
