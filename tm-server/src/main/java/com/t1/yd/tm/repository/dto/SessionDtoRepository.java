package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoSessionRepository;
import com.t1.yd.tm.dto.model.SessionDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionDtoRepository extends AbstractDtoUserOwnedRepository<SessionDTO> implements IDtoSessionRepository {

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }
}
