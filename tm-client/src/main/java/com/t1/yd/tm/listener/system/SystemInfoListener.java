package com.t1.yd.tm.listener.system;

import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.FormatUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "info";
    @NotNull
    public static final String ARGUMENT = "-i";
    @NotNull
    public static final String DESCRIPTION = "Show info about system";

    @Autowired
    public SystemInfoListener(@NotNull final ITokenService tokenService,
                              @NotNull final IPropertyService propertyService) {
        super(tokenService, propertyService);
    }

    @Override
    @EventListener(condition = "@systemInfoListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println(getSystemInfoString());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    private String getProcessorsString() {
        @NotNull final int processors = Runtime.getRuntime().availableProcessors();
        return "Available processors (cores): " + processors;
    }

    @NotNull
    private String getFreeMemoryString() {
        final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        return "Free memory : " + freeMemoryFormat;
    }

    @NotNull
    private String getMaxMemoryString() {
        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        return "Maximum memory : " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat);
    }

    @NotNull
    private String getTotalMemoryString() {
        final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        return "Total memory : " + totalMemoryFormat;
    }

    @NotNull
    private String getUsedMemoryString() {
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        return "Used memory : " + usedMemoryFormat;
    }

    @NotNull
    public String getSystemInfoString() {

        return getProcessorsString() +
                "\n" +
                getFreeMemoryString() +
                "\n" +
                getMaxMemoryString() +
                "\n" +
                getTotalMemoryString() +
                "\n" +
                getUsedMemoryString();

    }

}
