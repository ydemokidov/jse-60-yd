package com.t1.yd.tm.component;

import com.t1.yd.tm.api.service.IReceiverService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Bootstrap {

    private final IReceiverService receiverService;

    @Autowired
    public Bootstrap(@NotNull final IReceiverService receiverService) {
        this.receiverService = receiverService;
    }

    @PostConstruct
    public void init() {
        @NotNull final ActiveMQConnectionFactory activeMqConnectionFactory = new ActiveMQConnectionFactory("failover://tcp://127.0.0.1:61616");
        activeMqConnectionFactory.setTrustAllPackages(true);
        receiverService.receive();
    }

}
